<?php

namespace App\Service;

use Psr\Cache\CacheItemInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Cache\Adapter\MemcachedAdapter;
use Symfony\Component\Cache\CacheItem;
use DateInterval;

class RatingCacheService
{
    // better move constants to a config

    private const RATING_CACHE_KEY_PATTERN = 'ca_rating_uuid_%s';

    private const CACHE_LIFETIME = '1 hour';

    /**
     * @var MemcachedAdapter
     */
    protected $cachePool;

    public function __construct(MemcachedAdapter $cachePool)
    {
        $this->cachePool = $cachePool;
    }

    /**
     * Returns cache item retrieved by Uuid
     *
     * @param Uuid $uuid
     *
     * @return mixed|CacheItem
     */
    public function getRatingCacheItemByUuid(Uuid $uuid)
    {
        $key = $this->getRatingKeyByUuid($uuid);

        return $this->cachePool->getItem($key);
    }

    /**
     * Stores cache item and returns whether the operation was successful
     *
     * @param CacheItemInterface $cacheItem
     *
     * @return bool
     */
    public function storeRatingCacheItem(CacheItemInterface $cacheItem): bool
    {
        $cacheItem->expiresAfter(DateInterval::createFromDateString(self::CACHE_LIFETIME));

        return $this->cachePool->save($cacheItem);
    }

    /**
     * Returns cache key
     *
     * @param Uuid $uuid
     *
     * @return string
     */
    protected function getRatingKeyByUuid(Uuid $uuid): string
    {
        return sprintf(self::RATING_CACHE_KEY_PATTERN, $uuid->toString());
    }
}
