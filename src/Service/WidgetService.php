<?php

namespace App\Service;

use App\Exception\HotelNotFound;
use Doctrine\ORM\EntityRepository;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\InvalidUuidStringException;
use Doctrine\ORM\NonUniqueResultException;

class WidgetService
{
    protected const DEFAULT_WIDGET_VERSION_PREFIX = 'v1';

    /**
     * @var EntityRepository
     */
    protected $hotelRepository;

    /**
     * @var RatingCalculator
     */
    protected $ratingCalculator;

    public function __construct(EntityRepository $hotelRepository, RatingCalculator $ratingCalculator)
    {
        $this->hotelRepository  = $hotelRepository;
        $this->ratingCalculator = $ratingCalculator;
    }

    /**
     * Returns rating retrieved by hotel uuid
     *
     * @param string $uuid
     *
     * @return int
     * @throws HotelNotFound
     */
    public function getRatingByHotelUuidValue(string $uuid): int
    {
        try {
            $uuid = Uuid::fromString($uuid);
        } catch (InvalidUuidStringException $e) {
            throw new HotelNotFound('Hotel not found!');
        }

        $hotel = $this->hotelRepository->findOneBy(['uuid' => $uuid]);

        if (!$hotel) {
            throw new HotelNotFound('Hotel not found!');
        }

        return $this->ratingCalculator->getCalculatedRatingByHotel($hotel);
    }

    public function getDefaultVersion(): string
    {
        return self::DEFAULT_WIDGET_VERSION_PREFIX;
    }
}
