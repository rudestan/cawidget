<?php

namespace App\Service;

use Doctrine\ORM\EntityRepository;
use App\Entity\Hotel as HotelEntity;
use App\Exception\HotelNotFound;

class HotelService
{
    /**
     * @var EntityRepository
     */
    protected $entityRepository;

    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    /**
     * Returns some random hotel entity
     *
     * @return HotelEntity
     */
    public function getRandomHotel(): HotelEntity
    {
        $randomId = $this->getRandomHotelId();

        return $this->entityRepository->find($randomId);
    }

    /**
     * @param string $uuid
     *
     * @return null|object
     * @throws HotelNotFound
     */
    public function getHotelByUiid(string $uuid)
    {
        return $this->entityRepository->findOneBy(['uuid' => $uuid]);

        if (!$hotel) {
            throw new HotelNotFound('Hotel not found!');
        }

        return $hotel;
    }

    /**
     * Returns random number withing hardcoded id range
     *
     * @return int
     */
    protected function getRandomHotelId()
    {
        return rand(1, 5);
    }
}
