<?php

namespace App\Service;

use App\Entity\Hotel;
use App\Repository\ReviewRepository;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;

class RatingCalculator
{
    /**
     * @var ReviewRepository
     */
    protected $entityRepository;

    /**
     * @var RatingCacheService
     */
    protected $ratingCache;

    public function __construct(ReviewRepository $entityRepository, RatingCacheService $ratingCache)
    {
        $this->entityRepository = $entityRepository;
        $this->ratingCache      = $ratingCache;
    }

    /**
     * Returns rating
     *
     * @param Hotel $hotel
     *
     * @return float|int
     */
    public function getCalculatedRatingByHotel(Hotel $hotel)
    {
        $cacheItem = $this->ratingCache->getRatingCacheItemByUuid($hotel->getUuid());

        if ($cacheItem->isHit()) {
            return $cacheItem->get();
        }

        $rating = $this->calculateByHotel($hotel);

        $this->ratingCache->storeRatingCacheItem($cacheItem->set($rating));

        return $rating;
    }

    /**
     * Performs calculation by hotel
     *
     * @param Hotel $hotel
     *
     * @return int
     * @throws NonUniqueResultException
     */
    public function calculateByHotel(Hotel $hotel): int
    {
        $currentYear = $this->getCurrentYear();
        $startDate   = $this->getStartDate($currentYear);
        $endDate     = $this->getEndDate($currentYear);

        $rating = $this->entityRepository->getAverageRatingByHotelIdBetweenDates(
            $hotel->getId(),
            $startDate,
            $endDate
        );

        if ($rating == null) {
            return 0;
        }

        return ceil($rating);
    }

    /**
     * Returns current year
     *
     * @return string
     */
    protected function getCurrentYear()
    {
        $dateTime = new DateTime();

        return $dateTime->format('Y');
    }

    protected function getStartDate($currentYear)
    {
        $date = sprintf('%s-01-01', $currentYear);

        return DateTime::createFromFormat('Y-m-d', $date);
    }

    protected function getEndDate($currentYear)
    {
        $date = sprintf('%s-12-31', $currentYear);

        return DateTime::createFromFormat('Y-m-d', $date);
    }
}
