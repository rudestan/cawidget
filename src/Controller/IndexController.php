<?php

namespace App\Controller;

use App\Service\HotelService;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $hotel = $this->getHotelService()->getRandomHotel();
        $data  = [
            'own_uuid' => $hotel->getUuid(),
            'hotel'    => $hotel->getTitle(),
        ];

        return $this->render('index/index.html.twig', $data);
    }

    /**
     * Returns hotel service
     *
     * @return HotelService
     */
    protected function getHotelService(): HotelService
    {
        return $this->get('service.hotel');
    }
}
