<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Service\WidgetService;
use App\Exception\HotelNotFound;
use Symfony\Component\HttpFoundation\Response;

class WidgetController extends Controller
{
    const DEFAULT_WIDGET_VERSION = '';

    public function renderSnippet(string $uuid)
    {
        $widgetService   = $this->getWidgetService();
        $snippetTemplate = $this->getSnippetTemplateByVersion($widgetService->getDefaultVersion());

        try {
            $rating = $widgetService->getRatingByHotelUuidValue($uuid);
        } catch (HotelNotFound $e) { // if we did not find the hotel - just return empty string
            return new Response('');
        }

        return $this->render($snippetTemplate, ['rating' => $rating]);
    }

    /**
     * @param string $version
     *
     * @return string
     */
    protected function getSnippetTemplateByVersion(string $version): string
    {
        return sprintf('widget/%s/code_snippet.html.twig', $version);
    }

    /**
     * @return WidgetService
     */
    protected function getWidgetService(): WidgetService
    {
        return $this->get('service.widget');
    }
}
