<?php

namespace App\DataFixtures;

use App\Entity\Hotel;
use App\Entity\Review;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    /**
     * @var ObjectManager
     */
    protected $manager;

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        for ($i = 1; $i <= 5; $i++) {
            $hotel = new Hotel();
            $hotel->setTitle('Mega hotel ' . $i);
            $this->manager->persist($hotel);

            $this->addReviews($hotel);
        }

        $this->manager->flush();
    }

    protected function addReviews(Hotel $hotel)
    {
        for ($i = 0; $i < 10; $i++) {
            $review = new Review();
            $review->setHotel($hotel);
            $review->setRating(rand(1, 100));

            $this->manager->persist($review);
        }
    }
}
