<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;

class ReviewRepository extends EntityRepository
{
    /**
     * Returns hotel average rating between provided dates
     *
     * @param int      $hotelId
     * @param DateTime $startDate
     * @param DateTime $endDate
     *
     * @return null|float
     * @throws NonUniqueResultException
     */
    public function getAverageRatingByHotelIdBetweenDates(int $hotelId, DateTime $startDate, DateTime $endDate)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $query        = $queryBuilder
            ->select($queryBuilder->expr()->avg('r.rating'))
            ->from('App:Review', 'r')
            ->where('r.hotel = :hotel_id')
            ->andWhere('r.createdAt BETWEEN :start AND :end')
            ->getQuery();

        $query
            ->setParameter('hotel_id', $hotelId)
            ->setParameter('start', $startDate->format('Y-m-d'))
            ->setParameter('end', $endDate->format('Y-m-d'));

        $res = $query->getOneOrNullResult();

        if (isset($res[1])) {
            return $res[1];
        }

        return null;
    }
}
