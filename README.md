# README #

CA Widget simple test app.

UPD: Supports caching and added a little bit ugly CSS and a Unit test of the calculation service.

## Installation: ##

After cloning the current repo perform the following steps:

1. Run ``docker-compose up`` (composer should run automatically during first build)
2. Add the ``127.0.0.1  cawidget.dev`` and ``127.0.0.1   mysql`` lines to the ``etc/hosts`` file
to be able to access the host via ``http://cawidget.dev`` and also execute the DB migrations from host machine.
3. Copy ``.env.dist`` file to ``.env`` and modify the database URL to ``DATABASE_URL=mysql://ca_user:123456@mysql:3306/ca_widget_db``
4. Run ``php bin/console doctrine:schema:update --force`` from the directory of the project
5. Install fixtures by executing ``php bin/console doctrine:fixtures:load``
6. Open http://cawidget.dev