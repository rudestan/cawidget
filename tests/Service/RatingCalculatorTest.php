<?php

namespace App\Tests\Service;

use App\Entity\Hotel;
use App\Repository\ReviewRepository;
use App\Service\RatingCacheService;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Service\RatingCalculator;
use DateTime;
use Prophecy\Prophecy\ObjectProphecy;

class RatingCalculatorTest extends KernelTestCase
{
    /**
     * @var RatingCalculator
     */
    protected $sut;

    /**
     * @var ObjectProphecy|ReviewRepository
     */
    protected $reviewRepostioryProphecy;

    /**
     * @var ObjectProphecy|RatingCacheService
     */
    protected $ratingCacheProphecy;

    protected function setUp()
    {
        $this->reviewRepostioryProphecy = $this->prophesize(ReviewRepository::class);
        $this->ratingCacheProphecy      = $this->prophesize(RatingCacheService::class);
        $this->sut                      = new RatingCalculator(
            $this->reviewRepostioryProphecy->reveal(),
            $this->ratingCacheProphecy->reveal()
        );
    }

    /**
     * @return array
     */
    public function getCalculatedRatingByHotelCacheDataProvider()
    {
        return [
            [
                Uuid::uuid4(),
                10,
            ],
            [
                Uuid::uuid4(),
                58,
            ],
        ];
    }

    /**
     * @param Uuid $uuid
     * @param int  $expectedRating
     *
     * @dataProvider getCalculatedRatingByHotelCacheDataProvider
     */
    public function testGetCalculatedRatingByHotelCacheIsHit(Uuid $uuid, int $expectedRating)
    {
        $hotelProphecy = $this->prophesize(Hotel::class);
        $hotelProphecy->getUuid()->willReturn($uuid);

        $cacheItemProphecy = $this->prophesize('Psr\Cache\CacheItemInterface');
        $cacheItemProphecy->isHit()->willReturn(true);
        $cacheItemProphecy->get()->willReturn($expectedRating);

        $this->ratingCacheProphecy->getRatingCacheItemByUuid($uuid)->willReturn($cacheItemProphecy->reveal());
        $rating = $this->sut->getCalculatedRatingByHotel($hotelProphecy->reveal());

        $this->assertSame($expectedRating, $rating);
    }

    /**
     * @param Uuid $uuid
     * @param int  $expectedRating
     *
     * @dataProvider getCalculatedRatingByHotelCacheDataProvider
     */
    public function testGetCalculatedRatingByHotelCacheIsNotHit(Uuid $uuid, int $expectedRating)
    {
        $hotelProphecy = $this->prophesize(Hotel::class);
        $hotelProphecy->getUuid()->willReturn($uuid);
        $hotelProphecy->getId()->willReturn(123);

        $cacheItemProphecy = $this->prophesize('Psr\Cache\CacheItemInterface');
        $cacheItemProphecy->isHit()->willReturn(false);
        $cacheItemProphecy->set($expectedRating)->willReturn($cacheItemProphecy);

        $this->reviewRepostioryProphecy->getAverageRatingByHotelIdBetweenDates(
            123,
            $this->getStartDate(),
            $this->getEndDate()
        )->willReturn($expectedRating);

        $this->ratingCacheProphecy->getRatingCacheItemByUuid($uuid)->willReturn($cacheItemProphecy->reveal());
        $this->ratingCacheProphecy->storeRatingCacheItem($cacheItemProphecy)->shouldBeCalled(1);
        $rating = $this->sut->getCalculatedRatingByHotel($hotelProphecy->reveal());

        $this->assertSame($expectedRating, $rating);
    }

    /**
     * Data provider for testCalculateByHotel
     *
     * @return array
     */
    public function calculateByHotelDataProvider()
    {
        return [
            [
                10,
                10,
            ],
            [
                null,
                0,
            ],
            [
                58.2,
                58,
            ],
            [
                33.44,
                33,
            ],
        ];
    }

    /**
     * Tests whether calculateByHotel returns correct data
     *
     * @param int|null $returnedRating
     * @param int|null $expectedRating
     *
     * @dataProvider calculateByHotelDataProvider
     */
    public function testCalculateByHotel(?int $returnedRating, ?int $expectedRating)
    {
        $hotelProphecy = $this->prophesize(Hotel::class);
        $hotelProphecy->getId()->willReturn(123);

        $this->reviewRepostioryProphecy->getAverageRatingByHotelIdBetweenDates(
            123,
            $this->getStartDate(),
            $this->getEndDate()
        )->willReturn($returnedRating);

        $rating = $this->sut->calculateByHotel($hotelProphecy->reveal());

        $this->assertSame($expectedRating, $rating);
    }

    /**
     * Returns start date
     *
     * @return DateTime
     */
    protected function getStartDate(): DateTime
    {
        $date = sprintf('%s-01-01', date('Y'));

        return DateTime::createFromFormat('Y-m-d', $date);
    }

    /**
     * Returns end date
     *
     * @return DateTime
     */
    protected function getEndDate(): DateTime
    {
        $date = sprintf('%s-12-31', date('Y'));

        return DateTime::createFromFormat('Y-m-d', $date);
    }
}
